toolchain
=========

OSX opensource toolchain

## prerequirement
1. xcode install
 - xcode compiler (cli base)
 - xcode-select --install
2. download source code
 - gcc requires three other libraries: GMP, MPFR, MPC
 - gcc-4.9.1.tar.bz2 86MB [gcc](http://www.netgull.com/gcc/releases/gcc-4.9.1/gcc-4.9.1.tar.bz2)
 - gmp-6.0.0a.tar.bz2 2.2MB [gmp](https://ftp.gnu.org/gnu/gmp/gmp-6.0.0a.tar.bz2)
 - mpc-1.0.2.tar.gz 0.6MB [mpc](ftp://ftp.gnu.org/gnu/mpc/mpc-1.0.2.tar.gz)
 - mpfr-3.1.2.tar.bz2 1.2MB [mpfp](http://mpfr.loria.fr/mpfr-current/mpfr-3.1.2.tar.bz2)

## set up the source tree
1. decompress gcc-4.9.1
 - tar -xjvf gcc-4.9.1.tar.bz2
 - cd gcc-4.9.1
2. decompress gmp
 - tar -xjvf ../gmp-6.0.0a.tar.bz2
 - ln -s gmp-6.0.0 gmp
3. decompress mpc
 - tar -xzf ../mpc-1.0.2.tar
 - ln -s mpc-1.0.2/ mpc
4. decompress mpfr
 - tar -xjf ../mpfr-3.1.2.tar.bz2
 - ln -s mpfr-3.1.2 mpfr

## compile enviroment setting
- mkdir obj
- cd obj
- pwd
 - 00_toolchain/gcc_4.9.1/obj
- ../configure --prefix=/usr/local/gcc-4.9.1 --enable-languages=c,c++

## build up
- make -j4
- sudo make install

## enviroment setting
- check version
 - /usr/local/bin/gcc --version
 - export PATH=/usr/local/bin:$PATH
 
# cmake
- download source code
 - [cmake 3.0.0](http://www.cmake.org/files/v3.0/cmake-3.0.0.tar.gz)
 - mv ~/Downloads/cmake-3.0.0.tar .
 - tar xvf cmake-3.0.0.tar
 - cd cmake-3.0.0
 - mkdir obj
 - cd obj
 - ../bootstrap
 - make -j4
 - sudo make install
 
# wget
- download source code
 - [wget 1.15](http://ftp.gnu.org/gnu/wget/wget-1.15.tar.gz)
 - mv ~/Downloads/wget-1.15.tar .
 - tar xvf wget-1.15.tar
 - cd wget-1.15
 - mkdir obj
 - cd obj
 - ../configure --with-ssl=openssl
 - make -j4
 - sudo make install
 
# autoconf
- curl -OL http://ftpmirror.gnu.org/autoconf/autoconf-2.69.tar.gz
- mv ~/Downloads/autoconf-2.69.tar .
- tar xvf autoconf-2.69.tar 
- cd autoconf-2.69
- mkdir obj
- cd obj
- ../configure 
- make -j4
- sudo make install

# emacs
- download soruce code
- [emacs 24.3](http://ftp.gnu.org/pub/gnu/emacs/emacs-24.3.tar.gz)
- mv ~/Downloads/emacs-24.3.tar .
- tar xvf emacs-24.3.tar
- wget http://svn.sourceforge.jp/svnroot/macemacsjp/inline_patch/trunk/emacs-inline.patch
- cd emacs-24.3
- patch -p0 < ../emacs-inline.patch
- mkdir obj
- cd obj
- ../configure --with-ns --without-x
- make -j4
- sudo make install
- sudo rm /usr/bin/emacs
- sudo rm -rf /usr/share/emacs
- create an alias '.~/.bash_profile'
- alias emacs="/usr/local/bin/emacs -nw"

## git
- curl "https://www.kernel.org/pub/software/scm/git/git-2.0.4.tar.gz"
- mv ~/Downloads/git-2.0.4.tar .
- tar xvf git-2.0.4.tar
- cd git-2.0.4
- make configure
- make -j4
- sudo make install
- sudo rm /usr/bin/git

### git 사용법
## 이름, 메일 색인설정
```
$ git config --global user.name "MS Cho"
$ git config --global user.email hephaex@gmail.com
$ git config --global color.ui true
```

### 현재 설정 확인
- git config --list

### commit log 편집용 editor설정
- git config --global core.editor 'emacs'

### git 기본 명령어들
#### status 현재 상태을 확인
git status
#### add
- file을 스테징 영역으로 추가
- git add <file>

#### commit
- file을 로컬 저장서오 추가, 커밋 메시지 추가
- git commit -m "message"

#### reset
- 바로 전 커밋을 삭제
- git reset HEAD

#### revert
- 작업 트리를 지정한 커밋 시점으로 되돌림
- git revert <HashTag>

#### log
- 커밋 로크를 표시
- git log

#### merge
- 지역 브랜치를 머지
- git merge

#### rebase
- 브랜치를 파생의 시작으로 바꿈
- git rebase <branch>

#### stash
- 현재 작업 트리의 상태를 임시로 보관
- git stash

#### reflog, cherry-pick
- HEAD로그를 표시 commit 추출
- git reflog
- git cherry-pick <HashTag>

### 참조자료
[git, github 입문](http://www.slideshare.net/tomokis2/git-practice)
```
/* Set up Git Configuration */

git config --global user.email "neil@coolestguidesontheplanet.com"

git config --global user.name "Neil Gee"

git config --global core.editor "vi"

git config --global color.ui true

/* See Git configuration */

git config --list

/*  to initialise a local repository */

git init

/*  adds a file to the repo */

git add

/* commit the change to git */

git commit -m "Message goes here"

/*  see the commits */

git log

/*  Git has a 3 Tier Architecture:  Working - Staging Index - Respository

Changes to files are put in a Checksum SHA-1 hash 40digit value containing parent hash, author and message.

HEAD is the latest commit of the checked out branch */

/*  Basic Commands  */

git status  /*  the command 'git status' tells which files are not added or committed from Working to Staging to Repository */

git commit -m "" /*  Commits and changes to all files that are in Staging Tier into Repository  */

git diff /*  show changes between Working and Repository, no file supplied shows all files  */

git diff --staged /*  shows changes between Staged and Respository  */

git rm file.txt /*  will remove file from working then git commit -m "" to move to Repo */

git mv /*  rename or move files - then git commit -m "" to move to Repo */

git commit -am "text goes here" /* adds all files straight to Repo from Staging if they have changes - meaning they skip git add */

git checkout -- file.txt /*  restore Repo file to Working Directory using current branch  */

git reset HEAD file.txt /*  Move a Stage file out of Stage back to Working */

git commit --amend -m "message" file.txt /* Change last commit to Repo (only last one can change) */

/* Reverting --soft --mixed --hard */
git log /* gets the sha1s so you can see the coomits where you want to back to  */

git reset --soft sha /* changes Repo but not STaging or Working */

git reset --mixed sha /* changes Repo and STaging but not Working */

git reset --hard sha /* changes all 3 Tiers */

git clean -f /* remove untracked files from Working  */

.gitignore /* ignores files to track in Working / track the .gitignore file */

Global Ignore /* create in home folder  */
.gitignore_global
/* Add in  */
.DS_Store
.Trashes
.Spotlight_V100



git config --global core.excludesfile ~/.gitignore_global /* add to gitconfig */

/* Stop tracking changes */

rm --cached file.txt /* leaves copy in Repo and Working */


/* Track Folders changes

Add an invisble file to a folder like .gitkeeper then add and commit */

/* Commit Log  */
git ls-tree HEAD
git ls-tree master
git log --oneline
git log --author="Neil"
git log --grep="temp"

/* Show Commits */

git show dc094cb /*  show SHA1 */

/* Compare Commits

Branches */

git branch /*  Show local branches * is the one we are on */

git branch -r /* Shows remote branches */

git branch -a /* Shows local and remote */

git branch newbranch /* creates a new branch */

git checkout newbranch /* switch to new branch */

git checkout -b oldbranch /* creates and switches to new branch  */


/* Diff in Branches */

git diff master..otherbranch /*  shows diff */

git diff --color-words master..otherbranch /*  shows diff in color */

git branch --merged /*  shows any merged branches */

/* Rename Branch */

git branch -m oldname newname

/* Delete  Branch */

git branch -d nameofbranch

/* Merge Branch  */

git merge branchname /* be on the receiver branch */

/* Merge Conflicts between the same file on 2 branches are marked in HEAD and other branch */

git merge --abort /*  Abort basically cancels the merge */

/* Manually Fix Files and commit

The Stash */

git stash save "text message here"

git stash list /* shows whats in stash */

git stash show -p stash@{0} /* Show the diff in the stash */

git stash pop stash@{0} /*  restores the stash deletes the tash */

git stash apply stash@{0} /*  restores the stash and keeps the stash */

git stash clear /*  removes all stash */

git stash drop stash@{0}

/* Remotes
You fetch from the remote server, merge any differences - then push any new to the remote - 3 branches work remote server branch, local origin master and local master

Create a repo in GitHub, then add that remote to your local repo */

git remote add origin https://github.com/neilgee/genesischild.git /*  origin can be named whatever followed by the remote */

git remote /* to show all remotes */

git remote remove origin /* to remove remote */

git remote rm origin /* to remove remote */

/* Push to Remote from Local */

git push -u origin master

/* Cloning a GitHub Repo - create and get the URL of a new repository from GitHub, then clone that to your local repo, example below uses local repo named 'nameoffolder' */

git clone https://github.com/neilgee/genesischild.git nameoffolder

/* Push to Remote from Local - more - since when we pushed the local to remote we used -u parameter then the remote branch is tracked to the local branch and we just need to use... */

git push

/* Fetch changes from a cloned Repo */

git fetch origin /*  Pulls down latest committs to origin/master not origin, also pull down any branches pushed to Repo

Fetch before you work
Fetch before you pull
Fetch often */

/* Merge with origin/master */

git merge origin/master

/* git pull = git fetch + git merge

Checkout/Copy a remote branch to local */

git branch branchname origin/branchname /*  this will bring the remote branch to local and track with the remote */

/* Delete branch */

git branch -d branchname

/* Checkout and switch branch and track to remote */

git checkout -b nontracking origin/nontracking

/* Remove remote branch */

git push origin --delete branch
```

## cscope
- source code [cscope-15.8a](http://downloads.sourceforge.net/project/cscope/cscope/15.8a/cscope-15.8a.tar.gz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fcscope%2Ffiles%2F&ts=1407129629&use_mirror=jaist)
- mv ~/Download/cscope-15.8a.tar .
- tar xvf cscope-15.8a.tar
- cd cscope-15.8a
- emacs src/constants.h

```c
#if (BSD || V9 ) && !__NetBSD__ && !__FreeBSD__
```


```c
#if (BSD || V9 ) && !__NetBSD__ && !__FreeBSD__ && !__APPLE__
```
- ./configure
- make -j4
- sudo make install

## doxygen
- source code [doxygen-1.8.7](http://ftp.stack.nl/pub/users/dimitri/doxygen-$
- tar xvf doxygen-1.8.7
- cd doxygen-1.8.7
- configure
- make -j4
- sudo make install

## graphviz
- source down [graphviz-2.38.0](http://www.graphviz.org/pub/graphviz/stable/$
- tar xvf graphviz-2.38.0.tar
- cd graphviz-2.38.0
- ./configure
- make -j4
- sudo make install 

## magit
- wget https://github.com/downloads/magit/magit/magit-1.2.0.tar.gz
- tar -xf magit-1.2.0.tar.gz
- cd magit-1.2.0
- make
- sudo make install
- (add-to-list 'load-path "/usr/local/share/emacs/site-lisp/magit")
- (require 'magit)

- 실행방법
 - M-x magit-status
 - 수정할 부분에서 s를 누르면 add가 된다.
 - modified 가 되면 c를 누르고 commit -m 을 입력한다.
 - C-c C-c를 입력하면 push가 된다.

- 기능 조작 방법
|조작커맨드|기능|
|i|.gitignore에 파일 추가|
|k|물리 파일 지우기|
|S| 모든 파일 add (git add -A)|
|u|add 대상에서 빠지기|
|I|로그를 보기|
|L|자세한 로그를 보기|
|v|커밋 취소|
|F|git pull|
|P|git push|
|m|git merge|
|t|tag만들기|

- [사용자 메뉴얼](http://philjackson.github.com/magit/magit.html)

## node.js
- source [node.js](http://nodejs.org/dist/v0.10.31/node-v0.10.31.tar.gz)
- tar -zxvf node-v0.10.31.tar.gz
- cd node-v0.10.31
- ./configure
- make
- sudo make install
