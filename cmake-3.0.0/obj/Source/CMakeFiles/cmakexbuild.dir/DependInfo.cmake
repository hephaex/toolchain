# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/Source/cmakexbuild.cxx" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Source/CMakeFiles/cmakexbuild.dir/cmakexbuild.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "CMAKE_BUILD_WITH_CMAKE"
  "CURL_STATICLIB"
  "LIBARCHIVE_STATIC"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Source/CMakeFiles/CMakeLib.dir/DependInfo.cmake"
  "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Source/kwsys/CMakeFiles/cmsys.dir/DependInfo.cmake"
  "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Utilities/cmexpat/CMakeFiles/cmexpat.dir/DependInfo.cmake"
  "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Utilities/cmlibarchive/libarchive/CMakeFiles/cmlibarchive.dir/DependInfo.cmake"
  "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Utilities/cmbzip2/CMakeFiles/cmbzip2.dir/DependInfo.cmake"
  "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Utilities/cmcompress/CMakeFiles/cmcompress.dir/DependInfo.cmake"
  "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Utilities/cmcurl/CMakeFiles/cmcurl.dir/DependInfo.cmake"
  "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Utilities/cmzlib/CMakeFiles/cmzlib.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "Utilities"
  "../Utilities"
  "Source"
  "../Source"
  "Utilities/cmcompress"
  "../Source/CTest"
  "../Source/CursesDialog/form"
  "Source/CursesDialog/form"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
