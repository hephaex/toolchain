# CMake generated Testfile for 
# Source directory: /Users/mscho/Simon/00_toolchain/cmake-3.0.0/Tests/CMakeTests
# Build directory: /Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(CMake.List "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/ListTest.cmake")
add_test(CMake.VariableWatch "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/VariableWatchTest.cmake")
add_test(CMake.Include "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/IncludeTest.cmake")
add_test(CMake.FindBase "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/FindBaseTest.cmake")
add_test(CMake.Toolchain "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/ToolchainTest.cmake")
add_test(CMake.GetFilenameComponentRealpath "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/GetFilenameComponentRealpathTest.cmake")
add_test(CMake.Version "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/VersionTest.cmake")
add_test(CMake.Message "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/MessageTest.cmake")
add_test(CMake.File "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/FileTest.cmake")
add_test(CMake.ConfigureFile "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/ConfigureFileTest.cmake")
add_test(CMake.SeparateArguments "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/SeparateArgumentsTest.cmake")
add_test(CMake.ImplicitLinkInfo "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/ImplicitLinkInfoTest.cmake")
add_test(CMake.ModuleNotices "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/ModuleNoticesTest.cmake")
add_test(CMake.GetProperty "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/GetPropertyTest.cmake")
add_test(CMake.If "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/IfTest.cmake")
add_test(CMake.String "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/StringTest.cmake")
add_test(CMake.Math "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/MathTest.cmake")
add_test(CMake.CMakeMinimumRequired "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/CMakeMinimumRequiredTest.cmake")
add_test(CMake.CompilerIdVendor "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/CompilerIdVendorTest.cmake")
add_test(CMake.ProcessorCount "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-DKWSYS_TEST_EXE=/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Source/kwsys/cmsysTestsCxx" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/ProcessorCountTest.cmake")
add_test(CMake.PushCheckState "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/PushCheckStateTest.cmake")
add_test(CMake.While "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/WhileTest.cmake")
add_test(CMake.CMakeHostSystemInformation "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/CMakeHostSystemInformationTest.cmake")
add_test(CMake.FileDownload "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/FileDownloadTest.cmake")
set_tests_properties(CMake.FileDownload PROPERTIES  PASS_REGULAR_EXPRESSION "file already exists with expected MD5 sum")
add_test(CMake.FileDownloadBadHash "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/FileDownloadBadHashTest.cmake")
set_tests_properties(CMake.FileDownloadBadHash PROPERTIES  WILL_FAIL "TRUE")
add_test(CMake.FileUpload "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/FileUploadTest.cmake")
add_test(CMake.EndStuff "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-Ddir:STRING=/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/EndStuffTest" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/EndStuffTest.cmake")
add_test(CMake.GetPrerequisites "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-DCTEST_CONFIGURATION_TYPE:STRING=\${CTEST_CONFIGURATION_TYPE}" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/GetPrerequisitesTest.cmake")
add_test(CMake.PolicyCheck "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-DCMake_BINARY_DIR:PATH=/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj" "-DCMake_SOURCE_DIR:PATH=/Users/mscho/Simon/00_toolchain/cmake-3.0.0" "-DGIT_EXECUTABLE:STRING=/usr/bin/git" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/PolicyCheckTest.cmake")
add_test(CMake.CheckSourceTree "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/bin/cmake" "-DCMake_BINARY_DIR:PATH=/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj" "-DCMake_SOURCE_DIR:PATH=/Users/mscho/Simon/00_toolchain/cmake-3.0.0" "-DGIT_EXECUTABLE:STRING=/usr/bin/git" "-DHOME:STRING=/Users/mscho" "-P" "/Users/mscho/Simon/00_toolchain/cmake-3.0.0/obj/Tests/CMakeTests/CheckSourceTreeTest.cmake")
